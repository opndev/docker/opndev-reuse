# SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesley@opndev.io>
#
# SPDX-License-Identifier: BSD-3-Clause

FROM fsfe/reuse:latest

LABEL io.opndev.vendor="OPN Development"
LABEL io.opndev.label-with-value="reuse"
LABEL version="1.0"
LABEL description="Custom reuse image"
LABEL maintainer="Wesley Schwengle <wesley@opndev.io>"

RUN apk add git && git config --global --add safe.directory /data
